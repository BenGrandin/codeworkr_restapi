const User = require("../models/user"),
	Car = require("../models/car");

module.exports = {
	// Validation done
	index: async (req, res /*, next */) => {
		const users = await User.find({});
		res.status(200).json(users);
	},

	// Validation done
	newUser: async (req, res /*, next */) => {
		const newUser = new User(req.value.body),
			user = await newUser.save();
		res.status(201).json(user);
	},

	// Validation done
	getUser: async (req, res /*, next */) => {
		const { userId } = req.value.params,
			user = await User.findById(userId);

		res.status(200).json(user);
	},

	// Validation done
	replaceUser: async (req, res /*, next */) => {
		//enforce that body must contains all the fields
		const { userId } = req.value.params,
			newUser = req.value.body;
			
		await User.findByIdAndUpdate(userId, newUser);

		res.status(200).json({ succes: true });
	},

	// Validation done
	updateUser: async (req, res /*, next */) => {
		// req.body may contain any number of fields
		const { userId } = req.value.params,
			newUser = req.value.body;

		await User.findByIdAndUpdate(userId, newUser);

		res.status(200).json({ succes: true });
	},

	// Validation done
	getUsersCars: async (req, res /* next */) => {
		const { userId } = req.value.params,
			user = await User.findById(userId).populate("cars");
		res.status(200).json(user.cars);
	},
	// Validation done
	newUserCar: async (req, res /* next */) => {
		const { userId } = req.value.params,
			newCar = new Car(req.value.body),
			user = await User.findById(userId);

		// Assign user as a car's seller
		newCar.seller = user;
		// Save newCar
		await newCar.save();
		// Add car to the user's selling array "cars"
		user.cars.push(newCar);
		// Save user
		await user.save();

		res.status(201).json(newCar);
	}
};
