const Car = require("../models/car.js");
const User = require("../models/user.js");

module.exports = {
	index: async (req, res /* next */) => {
		const cars = await Car.find({});

		res.status(200).json(cars);
	},

	newCar: async (req, res /* next */) => {
		// 1 Find the seller
		const seller = await User.findById(req.value.body.seller),
			// 2 Create a new Car
			newCar = req.value.body;

		delete newCar.seller;

		const car = new Car(newCar);
		car.seller = seller;
		await car.save();

		// 3 Add car to seller
		seller.cars.push(car);
		await seller.save();

		res.status(200).json(car);
	},

	getCar: async (req, res /* next */) => {
		const car = await Car.findById(req.value.params.carId);
		res.status(200).json(car);
	},

	replaceCar: async (req, res /* next */) => {
		const { carId } = req.value.params,
			newCar = req.value.body;

		await Car.findByIdAndUpdate(carId, newCar);
		res.status(200).json({ succes: true });
	},

	updateCar: async (req, res /* next */) => {
		const { carId } = req.value.params,
			newCar = req.value.body;

		await Car.findByIdAndUpdate(carId, newCar);
		res.status(200).json({ succes: true });
	},

	deleteCar: async (req, res /* next */) => {
		const { carId } = req.value.params,
			car = await Car.findById(carId);

		if (!car) return res.status(404).json({ error : "Car doesn't exist" });

		const seller = await User.findById(car.seller);

		await car.remove();
		seller.cars.pull(car);
		await seller.save();

		res.status(200).json({ succes: true });
	}
};
