const mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    userSchema = new Schema({
        firstName: {
            type: String
        },
        lastName: String,
        email: String,
        cars: [
            {
                type: Schema.Types.ObjectId,
                ref: "car"
            }
        ]
    }),
    User = mongoose.model("user", userSchema);
module.exports = User;
